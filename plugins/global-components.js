import Vue from 'vue'

import LoadingIcon from '~/components/common/loading-icon'
Vue.component('LoadingIcon', LoadingIcon)

import ChampInfo from '~/components/champ-info'
Vue.component('ChampInfo', ChampInfo)
